﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using PatientsRegistry.Helpers.Attributes;

namespace PatientsRegistry.Models
{
    public class NewPatientModel
    {
        [Required]
        [CyrillicOnly]
        [StringLength(128)]

        public string FirstName { get; set; }
        [Required]
        [CyrillicOnly]
        [StringLength(128)]

        public string LastName { get; set; }
        [Required]
        [CyrillicOnly]
        [StringLength(128)]
        public string MiddleName { get; set; }

        [Required]
        [CustomDate]
        public DateTime BirthdayDate { get; set; }

        [Required]
        [ValidateWithValues(new []{"мужчина", "женщина"})]
        public string Gender { get; set; }
        
        [Required]
        [StringLength(13, MinimumLength = 13)]
        [RegularExpression(@"^\+380\d{3}\d{2}\d{2}\d{2}$")]
        public string Phone { get; set; }

        public List<AdditionalContact> AdditionalContacts{get; set;}

        public bool Active{get; set; }
    }

    public class AdditionalContact
    {
        [ValidateWithValues(new[] { "рабочий", "домашний", "в экстренном случае" })]
        public string ContactType { get; set; }
        [StringLength(13, MinimumLength = 13)]
        [RegularExpression(@"\+?[0-9]{13}")]
        public string Phone { get; set; }
    }

}
