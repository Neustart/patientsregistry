﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nest;

namespace PatientsRegistry.Models.NestModels
{
    public class PatientNestModel
    {
        [Text(Analyzer = "russian")]
        public string FirstName { get; set; }
        [Text(Analyzer = "russian")]
        public string LastName { get; set; }
        [Text(Analyzer = "russian")]
        public string MiddleName { get; set; }
        [Date(Format = "dd-MM-yyyy")]
        public DateTime BirthdayDate { get; set; }
        [Keyword]
        public string Gender { get; set; }
        [Text]
        public string Phone { get; set; }
        [Nested]
        public List<AdditionalContact> AdditionalContacts { get; set; }
        [Boolean]
        public bool Active { get; set; }
    }

    public class AdditionalContact
    {
        public string ContactType { get; set; }
        public string Phone { get; set; }
    }
}
