﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PatientsRegistry.Models
{
    public class SearchInputModel
    {
        public string Text { get; set; }
    }
}
