﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PatientsRegistry.Helpers.Repositories.Interfaces;
using PatientsRegistry.Models;
using PatientsRegistry.Models.NestModels;

namespace PatientsRegistry.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SearchPatientController : ControllerBase
    {

        private readonly IPatientRepository _patientRepository;

        public SearchPatientController(IPatientRepository patientRepository)
        {
            _patientRepository = patientRepository;
        }

        // GET: api/SearchPatient/<text>
        [HttpGet("{textToSearch}", Name = "SearchPatient")]
        public IActionResult Post(string textToSearch)
        {
            var result = _patientRepository.SearchPatients(textToSearch, out bool isSuccess);
            return Ok(result);
        }
        [HttpPost]
        public IActionResult Post([FromBody] SearchInputModel input)
        {
            if (!string.IsNullOrEmpty(input.Text) && input.Text.Length >= 3 && input.Text.Length < 256)
            {
                var result = _patientRepository.SearchPatients(input.Text, out bool isSuccess);
                if (isSuccess)
                {
                    return Ok(result);
                }
                return StatusCode(StatusCodes.Status500InternalServerError, "Error");
            }
            return StatusCode(StatusCodes.Status400BadRequest, "Incorrect input");
        }
    }
}
