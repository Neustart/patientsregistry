﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PatientsRegistry.Helpers.Repositories.Interfaces;
using PatientsRegistry.Models;

namespace PatientsRegistry.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PatientController : ControllerBase
    {
        private readonly IPatientRepository _patientRepository;

        public PatientController(IPatientRepository patientRepository)
        {
            _patientRepository = patientRepository;
        }

        // GET: api/Patient/5
        [HttpGet("{id}", Name = "Get")]
        public IActionResult Get(string id)
        {
            //getting patient by id
            var response = _patientRepository.GetPatientById(id, out bool isSuccess);
            if (isSuccess)
            {
                return Ok(response);
            }
            return StatusCode(StatusCodes.Status500InternalServerError, response);
        }

        [HttpPost]
        public IActionResult Post([FromBody] NewPatientModel input)
        {
            //create new patient
            var response = _patientRepository.CreateNewPatient(input, out bool isSuccess);
            if (isSuccess)
            {
                return Ok(response);
            }
            return StatusCode(StatusCodes.Status500InternalServerError, "Error");
        }

        [HttpPut("{id}")]
        public IActionResult Put(string id, [FromBody] NewPatientModel input)
        {
            //edit patient
            var response = _patientRepository.EditPatientById(input, id, out bool isSuccess);
            if (isSuccess)
            {
                return Ok(response);
            }
            return StatusCode(StatusCodes.Status500InternalServerError, response);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            //set active parameter to false to patient
            var response = _patientRepository.DeactivatePatientById(id, out bool isSuccess);
            if (isSuccess)
            {
                return Ok(response);
            }
            return StatusCode(StatusCodes.Status500InternalServerError, "Error");
        }
    }
}
