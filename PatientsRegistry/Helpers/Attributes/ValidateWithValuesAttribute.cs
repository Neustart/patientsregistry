﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PatientsRegistry.Helpers.Attributes
{
    public class ValidateWithValuesAttribute : ValidationAttribute
    {
        string[] _names;

        public ValidateWithValuesAttribute(string[] names)
        {
            _names = names;
        }
        public override bool IsValid(object value)
        {
            if (value != null && !string.IsNullOrEmpty(value.ToString()))
            {
                if (_names.Any(x=> x.Equals(value.ToString()))) { 
                    return true;
                }
            }
            return false;
        }
    }
}
