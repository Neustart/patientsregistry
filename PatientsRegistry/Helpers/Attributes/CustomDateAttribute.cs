﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PatientsRegistry.Helpers.Attributes
{
    public class CustomDateAttribute : RangeAttribute
    {
        public CustomDateAttribute()
            : base(typeof(DateTime),
                DateTime.Parse("01.01.1900").ToShortDateString(),
                DateTime.Now.ToShortDateString())
        { }
    }
}
