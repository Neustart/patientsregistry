﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PatientsRegistry.Helpers.Attributes
{
    public class CyrillicOnlyAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value!=null && !string.IsNullOrEmpty(value.ToString()))
            {
                if (!Regex.IsMatch(value.ToString(), @"\P{IsCyrillic}"))
                {
                    // there are only cyrillic characters in the string
                    return true;
                }
            }
            return false;
        }
    }
}
