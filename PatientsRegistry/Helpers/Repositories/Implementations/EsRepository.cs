﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Nest;

namespace PatientsRegistry.Helpers.Repositories.Implementations
{
    public class EsRepository
    {
        private static ElasticClient _client;

        public EsRepository()
        {
            _client = GetInstance();
        }

        public static ElasticClient GetInstance()
        {
            if (_client == null)
            {
                //TODO: move to app settings
                Uri node = new Uri("https://search-defaultestdomain-3hu3xwoyi4pvgiuleepokonhaq.us-east-1.es.amazonaws.com");
                ConnectionSettings settings = new ConnectionSettings(node);
                settings.DefaultIndex("patients_esindex");
                _client = new ElasticClient(settings); 
            }
            return _client;
        }
    }
}
