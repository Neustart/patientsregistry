﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.Configuration;
using Nest;
using PatientsRegistry.Helpers.Repositories.Interfaces;
using PatientsRegistry.Models;
using PatientsRegistry.Models.NestModels;

namespace PatientsRegistry.Helpers.Repositories.Implementations
{
    public class EsPatientRepository : IPatientRepository
    {
        private readonly IMapper _mapper;
        private static ElasticClient _esClient;

        public EsPatientRepository(IMapper mapper)
        {
            _mapper = mapper;
            _esClient = EsRepository.GetInstance();
        }

        public string CreateNewPatient(NewPatientModel input, out bool isSuccess)
        {
            var model = _mapper.Map<PatientNestModel>(input);
            var response = _esClient.Index(model, idx => idx.Index("patients_esindex"));
            if (!string.IsNullOrEmpty(response.Id))
            {
                isSuccess = true;
                return response.Id;
            }
            isSuccess = false;
            return "Error";
        }

        public PatientNestModel GetPatientById(string id, out bool isSuccess)
        {
            var response = _esClient.Get<PatientNestModel>(id, idx => idx.Index("patients_esindex"));
            if (response.Source != null)
            {
                isSuccess = true;
                return response.Source;
            }
            isSuccess = false;
            return null;
        }

        public bool DeactivatePatientById(string id, out bool isSuccess)
        {
            var response = _esClient.Get<PatientNestModel>(id, idx => idx.Index("patients_esindex"));
            if (response.Source != null)
            {

                dynamic updateFields = new ExpandoObject();
                updateFields.Active = false;

                var updateResponse = _esClient.Update<PatientNestModel, dynamic>(new DocumentPath<PatientNestModel>(id),
                    u => u.Index("patients_esindex").Doc(updateFields));
                if (updateResponse.ServerError == null)
                {
                    isSuccess = true;
                    return true;
                }
               
            }
            isSuccess = false;
            return false;
        }

        public NewPatientModel EditPatientById(NewPatientModel input, string id, out bool isSuccess)
        {
            var response = _esClient.Get<PatientNestModel>(id, idx => idx.Index("patients_esindex"));
            if (response.Source != null)
            {
                dynamic updateFields = new ExpandoObject();
                updateFields.FirstName = input.FirstName;
                updateFields.LastName = input.LastName;
                updateFields.MiddleName = input.MiddleName;
                updateFields.BirthdayDate = input.BirthdayDate;
                updateFields.Phone = input.Phone;
                updateFields.Gender = input.Gender;
                updateFields.AdditionalContacts = input.AdditionalContacts;
               
                var updateResponse = _esClient.Update<PatientNestModel, dynamic>(new DocumentPath<PatientNestModel>(id),
                    u => u.Index("patients_esindex").Doc(updateFields));
                if (updateResponse.ServerError == null)
                {
                    isSuccess = true;
                    return input;
                }

            }
            isSuccess = false;
            return null;
        }

        public List<PatientNestModel> SearchPatients(string input, out bool isSuccess)
        {
            ISearchResponse<PatientNestModel> searchResponse;
            //check is this date or text
            if (IsThisDate(input))
            {
                //if date create request for target day
                searchResponse = _esClient.Search<PatientNestModel>(s => s
                    .Query(q => q
                        .Bool(b => b
                            .Must(bb => bb.DateRange(dt => dt.Field("birthdayDate")
                            .GreaterThanOrEquals(DateTime.Parse(input))
                            .LessThan(DateTime.Parse(input).AddDays(1))))))
                );

            }
            else
            {
                //else create query_string for neccessary fields
                searchResponse = _esClient.Search<PatientNestModel>(s => s
                    .Query(q => q
                        .Bool(b => b.Must(m => m
                            .QueryString(qq => qq.Fields(new[] { "firstName", "lastName", "middleName", "phone" }).Query(input + "*")))
                        ))
                );

            }
            if (searchResponse.IsValid)
            {
                var result = searchResponse.Hits.Select(x => x.Source).ToList();
                isSuccess = true;
                return result;
            }
            isSuccess = false;
            return null;
        }

        private bool IsThisDate(string date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
