﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PatientsRegistry.Models;
using PatientsRegistry.Models.NestModels;

namespace PatientsRegistry.Helpers.Repositories.Interfaces
{
    public interface IPatientRepository
    {
        string CreateNewPatient(NewPatientModel input, out bool isSuccess);
        PatientNestModel GetPatientById(string id, out bool isSuccess);
        bool DeactivatePatientById(string id, out bool isSuccess);
        NewPatientModel EditPatientById(NewPatientModel input, string id, out bool isSuccess);
        List<PatientNestModel> SearchPatients(string input, out bool isSuccess);
    }
}
