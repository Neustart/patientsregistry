﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using PatientsRegistry.Models;
using PatientsRegistry.Models.NestModels;

namespace PatientsRegistry
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<NewPatientModel, PatientNestModel>()
                .ForMember(x => x.Active, y => y.MapFrom(src => true));
        }
    }
}
